package pages;

import commonactions.CommonActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends CommonActions {
    public HomePage(WebDriver driver){ PageFactory.initElements(driver,this);}
    @FindBy(id="onetrust-accept-btn-handler")
    WebElement acceptCookiesBtn;
    @FindBy(id="from.search")
    WebElement fromStationInputBox;
    @FindBy(id="to.search")
    WebElement toStationInputBox;
    @FindBy(id="single")
    WebElement oneWayJourneyRadioBtn;
    @FindBy(id="page.journeySearchForm.outbound.title")
    WebElement outBoundDate;
    @FindBy(xpath = "//div[@class=\"_4q70sf\"]/select[@name=\"dateType\"]")
    WebElement dateTypeSelector;
    @FindBy(xpath = "//div[@class=\"_9mpaen7\"]/select[@aria-label=\"hour\"]")
    WebElement hourSelect;
    @FindBy(xpath = "//div[@class=\"_9mpaen7\"]/select[@aria-label=\"minutes\"]")
    WebElement minuteSelect;
    @FindBy(id="passenger-summary-btn")
    WebElement passengerSummaryBtn;
    @FindBy(name="adults")
    WebElement numberOfAdultsSelect;
    @FindBy(xpath = "//button[@type=\"button\"]//span[contains(text(),\"Done\")]")
    WebElement passengerSummaryConfirmBtn;
    @FindBy(xpath = "//button[@type=\"submit\"]//span[contains(text(),\"Get times\")]")
    WebElement getTicketBtn;

    public void clickAcceptCookiesBtn(){
        clickElement(acceptCookiesBtn);
    }
    public void inputFromStation(String fromStation){
        enterInput(fromStationInputBox,fromStation,"ENTER");
    }
    public void inputToStation(String toStation){
        enterInput(toStationInputBox,toStation,"ENTER");
    }
    public void clickOneWayRadioBtn(){
        clickElement(oneWayJourneyRadioBtn);
    }
    public void inputOutboundDate(String outDate){
        outBoundDate.clear();
        enterInput(outBoundDate,outDate,"ENTER");
    }
    public void selectDateType(String dateType){
        select(dateTypeSelector,dateType);
    }
    public void selectHour(String hour){
        select(hourSelect,hour);
    }
    public void selectMinute(String minute){
        select(minuteSelect,minute);
    }
    public void clickPassengerSummaryBtn(){
        clickElement(passengerSummaryBtn);
    }
    public void selectNumberOfAdults(String numberOfAdults){
        select(numberOfAdultsSelect,numberOfAdults);
    }
    public void clickPassengerSummaryConfirmBtn(){
        clickElement(passengerSummaryConfirmBtn);
    }
    public void clickGetTicketBtn(){
        clickElement(getTicketBtn);
    }
    public void inputJourneyDetails(String fromStation,String toStation,String outDate,String dateType,String hour,String minute,String numberOfAdults){
        clickAcceptCookiesBtn();
        inputFromStation(fromStation);
        inputToStation(toStation);
        clickOneWayRadioBtn();
        inputOutboundDate(outDate);
        selectDateType(dateType);
        selectHour(hour);
        selectMinute(minute);
        clickPassengerSummaryBtn();
        selectNumberOfAdults(numberOfAdults);
        clickPassengerSummaryConfirmBtn();
        clickGetTicketBtn();
    }

}
