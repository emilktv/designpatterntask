package pages;

import commonactions.CommonActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DeliveryOptionsPage extends CommonActions {
    public DeliveryOptionsPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }
    @FindBy(xpath = "//button[@title=\"Continue\"]")
    WebElement continueToCheckoutBtn;
    public void clickContinueToPaymentBtn(){
        clickElement(continueToCheckoutBtn);
    }
}
