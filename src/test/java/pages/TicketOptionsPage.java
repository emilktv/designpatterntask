package pages;

import commonactions.CommonActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class TicketOptionsPage extends CommonActions {
    public TicketOptionsPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }
    @FindBy(name = "direction")
    WebElement directionSelect;
    @FindBy(name="position")
    WebElement positionSelect;
    @FindBy(name = "carriageType")
    WebElement carriageTypeSelect;
    @FindBy(xpath = "//button[@title=\"Continue\"]")
    WebElement continueBtn;


    public void selectDirection(String direction){
        select(directionSelect,direction);
    }
    public void selectPosition(String position){
        select(positionSelect,position);
    }
    public void selectCarriageType(String carriageType){
        select(carriageTypeSelect,carriageType);
    }
    public void clickContinueBtn(){
        clickElement(continueBtn);
    }
    public void inputJourneyDetails(String direction,String position,String carriageType){
        selectDirection(direction);
        selectPosition(position);
        selectCarriageType(carriageType);
        clickContinueBtn();
    }

}
