package pages;

import commonactions.CommonActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends CommonActions {
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//input[@name=\"isGuest\"]//following::span[contains(text(),\"check out as guest\")]")
    WebElement isGuestRadioBtn;
    @FindBy(name = "email")
    WebElement guestEmailInputBox;
    @FindBy(xpath = "//button[@type=\"submit\"]")
    WebElement continueAsGuestBtn;

    public void clickIsGuestRadioBtn(){
        clickElement(isGuestRadioBtn);
    }
    public void inputGuestEmail(String guestEmail){
        enterInput(guestEmailInputBox,guestEmail);
    }
    public void clickContinueAsGuest(){
        clickElement(continueAsGuestBtn);
    }
    public void login(String guestEmail){
        clickIsGuestRadioBtn();
        inputGuestEmail(guestEmail);
        clickContinueAsGuest();
    }

}
