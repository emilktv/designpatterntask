package pages;

import commonactions.CommonActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TicketPage extends CommonActions {
    public TicketPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//button[@title=\"Continue\"]")
    WebElement continueBtn;
    public void selectContinueBtn(){
        clickElement(continueBtn);
    }
}

