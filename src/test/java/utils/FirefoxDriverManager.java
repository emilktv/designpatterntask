package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static io.github.bonigarcia.wdm.config.DriverManagerType.FIREFOX;

public class FirefoxDriverManager extends DriverFactory{
    public WebDriver init(){
        WebDriverManager.getInstance(FIREFOX).setup();
        return new FirefoxDriver();
    }
}
