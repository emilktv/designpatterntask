package utils;

import org.openqa.selenium.WebDriver;

public class DriverFactory {
    WebDriver driver;
    public WebDriver getDriver(String browserName){
        switch (browserName){
            case "chrome":
                driver=new ChromeDriverManager().init();
                break;
            case "firefox":
                driver= new FirefoxDriverManager().init();
                break;
            default:
                return null;
        }
        return driver;
    }
}
