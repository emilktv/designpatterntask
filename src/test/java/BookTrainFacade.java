import org.openqa.selenium.WebDriver;
import pages.*;

public class BookTrainFacade {
    WebDriver driver;
    HomePage homePage;
    TicketPage ticketPage;
    LoginPage loginPage;
    TicketOptionsPage ticketOptionsPage;
    DeliveryOptionsPage deliveryOptionsPage;

    public BookTrainFacade(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        if(homePage==null){
            homePage=new HomePage(driver);
            return homePage;
        }
        else
            return homePage;
    }

    public TicketPage getTicketPage() {
        if(ticketPage==null){
            ticketPage=new TicketPage(driver);
            return ticketPage;
        }
        return ticketPage;
    }

    public LoginPage getLoginPage() {
        if(loginPage==null){
            loginPage=new LoginPage(driver);
            return loginPage;
        }
        return loginPage;
    }

    public TicketOptionsPage getTicketOptionsPage() {
        if(ticketOptionsPage==null){
            ticketOptionsPage=new TicketOptionsPage(driver);
            return ticketOptionsPage;
        }
        return ticketOptionsPage;
    }

    public DeliveryOptionsPage getDeliveryOptionsPage() {
        if(deliveryOptionsPage==null){
            deliveryOptionsPage=new DeliveryOptionsPage(driver);
            return deliveryOptionsPage;
        }
        return deliveryOptionsPage;
    }

    public void bookTrainFacade(String fromStation,String toStation,String outDate,String dateType,String hour,String minute,String numberOfAdults,String guestEmail){
        getHomePage().inputJourneyDetails(fromStation, toStation, outDate, dateType, hour, minute, numberOfAdults);
        getTicketPage().selectContinueBtn();
        getLoginPage().login(guestEmail);
        //ticketOptionsPage.inputJourneyDetails(direction, position, carriageType);
        getDeliveryOptionsPage().clickContinueToPaymentBtn();
    }
}
