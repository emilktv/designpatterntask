package commonactions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

//Common Actions class to implement methods for click,sendKeys. Extends Waiter class to apply waits on elements

public class CommonActions {
        public void clickElement(WebElement element){
            element.click();
        }
        public void enterInput(WebElement element,String input){
            element.sendKeys(input);
        }
        public void enterInput(WebElement element,String input,String keyPress){
            if(keyPress.equals("ENTER")){
                element.sendKeys(input+Keys.ENTER);
            }
        }
        public void select(WebElement element,String value){
            Select select=new Select(element);
            select.selectByValue(value);
        }
}
