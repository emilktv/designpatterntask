import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.DriverFactory;

import java.util.concurrent.TimeUnit;

public class BookTrainTest {
    WebDriver driver;
    @DataProvider(name="booktraindetails")
    public Object[][] dataProvider(){
        return new Object[][]{
                {"London","Birmingham","15-Jul-21","arriveBefore","20","00","1","test@gmail.com"}
        };
    }
    @BeforeMethod
    public void setup(){
        String browserName=System.getProperty("browser");
        driver=new DriverFactory().getDriver(browserName);
        driver.get("https://www.thetrainline.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test(dataProvider = "booktraindetails")
    public void bookTrainTest(String fromStation,String toStation,String outDate,String dateType,String hour,String minute,String numberOfAdults,String guestEmail){
        new BookTrainFacade(driver).bookTrainFacade(fromStation, toStation, outDate, dateType, hour, minute, numberOfAdults, guestEmail);
        String result=driver.findElement(By.xpath("//li[@class=\"_1e4363h8\"]//span[contains(text(),\"Payment\")]")).getText();
        Assert.assertEquals(result,"Payment");
    }
    @AfterMethod
    public void tearDown(){
        driver.close();
    }
}
